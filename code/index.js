window.addEventListener("DOMContentLoaded", () => {
  // document.getElementById("btn-wrapper").addEventListener("keypress", (e) => {
  document.addEventListener("keydown", (e) => {
    console.dir(e);

    let btnStyle = function (key) {
      let otherBtn = document.querySelectorAll(".btn");
      otherBtn.forEach((element) => element.classList.remove("btnBlue"));
      let thisBtn = document.getElementById(key);
      thisBtn.classList.add("btnBlue");
    };

    switch (e.code) {
      case "Enter":
        btnStyle("enter");
        break;
      case "KeyS":
        btnStyle("s");
        break;
      case "KeyE":
        btnStyle("e");
        break;
      case "KeyO":
        btnStyle("o");
        break;
      case "KeyN":
        btnStyle("n");
        break;
      case "KeyL":
        btnStyle("l");
        break;
      case "KeyZ":
        btnStyle("z");
        break;
    }
  });
});
